public class WordList {
	WordMeaningNode list;
	
	public WordList()
	{
		list = null;
	}
	
	// Adds word meaning to the beginning of the list
	public void	prepend(WordMeaning wm)
	{
		WordMeaningNode temp = new WordMeaningNode(wm);
		temp.next = list;
		list = temp;
	}
	
	// Add word meaning to the end of the list
	public void	append(WordMeaning wm)
	{
		WordMeaningNode temp = new WordMeaningNode(wm);
		WordMeaningNode curr = list;
		WordMeaningNode back = null;
		
		if (curr == null) {
			list = temp;
		}
		else {
			while(curr != null)
			{
				back = curr;
				curr = curr.next;
			}
			back.next = temp;
		}
	}
	
	public void add(String word, String def) {
		WordMeaning wm = new WordMeaning(word,def);
		add(wm);
	}
	
	
	public void add(WordMeaning wm)
	{
		WordMeaningNode temp = new WordMeaningNode(wm);
		WordMeaningNode curr = list;
		WordMeaningNode back = null;
		
		if(curr == null) { // empty
			prepend(wm);
			
		}
		else {
			while(curr != null && temp.wordMeaning.word.compareTo(curr.wordMeaning.word) > 0) {
				back = curr;
				curr = curr.next;
			}
			
			if (back == null) { // temp is greater than the head of the list
				prepend(wm);
			
			}
			else { // temp belongs in the middle of the list or at the end
				back.next = temp;
				temp.next = curr;
			}
		}
	}
	
	public void remove(String key)
	{
		WordMeaningNode curr = list;
		WordMeaningNode back = null;
		
		if (curr == null) // Empty list, what are you trying to remove?
			return;
		
		// Found at the beginning of the list
		if (curr.wordMeaning.word.equalsIgnoreCase(key)) {
			list = curr.next;
			return;
		}

		back = curr;
		curr = curr.next;
		
		while (curr != null) {
			
			if (curr.wordMeaning.word.equalsIgnoreCase(key)) {
				back.next = curr.next;
				return;
			}
			
			back = curr;
			curr = curr.next;
		}
	}
	
	
	public static String replaceStringWithSpaces(String str)
	{
		char[] charArray = str.toCharArray();
		
		for(int i = 0; i < charArray.length; i++)
			charArray[i] = ' ';
		
		return new String(charArray);
	}
	
	public String toString()
	{
		String s = "";
		WordMeaningNode curr = list;
		WordMeaningNode back = null;
		
		if (curr == null) // empty list
			return "";
		
		s += curr.wordMeaning.word + " - " + curr.wordMeaning.wordMeaning + "\n";
		back = curr;
		curr = curr.next;
		
		while(curr != null)
		{
			if (back.wordMeaning.word.equalsIgnoreCase(curr.wordMeaning.word))
			{
				s += replaceStringWithSpaces(curr.wordMeaning.word) +
					" - " + curr.wordMeaning.wordMeaning + "\n";
			}
			else
			{
				s += curr.wordMeaning.word + " - " + curr.wordMeaning.wordMeaning + "\n";
			}
			
			back = curr;
			curr = curr.next;
		}
		
		return s;
	}
	
}