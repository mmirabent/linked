public class Dictionary {
	
	private static WordList wl, deletedWords;
	
	public static void main(String[] args) {
		
		boolean done = false;
		wl = new WordList();
		deletedWords = new WordList();
		int choice;
		
		while (!done) {
			try {
				choice = menu();
				
				switch (choice) {
					case 1:
						add();
						break;
					case 2:
						delete();
						break;
					case 3:
						displayDictionary();
						break;
					case 4:
						displayDeletedWords();
						break;
					case 0:
						done = true;
						break;
				}
			}
			catch (NumberFormatException nfe) {
				// The user entered something that's not a number
				// do nothing, print menu
			}
		}
		
	}
	
	private static int menu() {
		return Display.getInt("Menu\n1. Add a new dictionary entry\n2. Delete a dictionary entry\n3. Show the dictionary\n4. Show the deleted words\n0. Quit");
	}
	
	private static void add() {
		String word, definition;
		
		word = Display.getWord("Please enter the word");
		definition = Display.getWord("Please enter the definition of " + word);
		
		wl.add(word,definition);
	}
	
	private static void delete() {
		String key;
		
		key = Display.getWord("Please enter the word to be deleted");
		
		wl.remove(key);
		deletedWords.add(key,"");
	}
	
	private static void displayDictionary() {
		Display.display("Dictionary",wl.toString(),30,80);
	}
	
	private static void displayDeletedWords() {
		Display.display("Deleted Words",deletedWords.toString(),30,80);
	}
	
	
}