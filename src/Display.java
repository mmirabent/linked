import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import java.awt.Font;

class Display
{
	static double getDouble(String s)
	{
		return Double.parseDouble(getWord(s));
	}
	
	static int getInt(String s)
	{
		return Integer.parseInt(getWord(s));
	}
	
	static String getWord(String s)
	{
		return JOptionPane.showInputDialog(s);
	}
	
	static void displayErr(String topic, String msg)
	{
		JOptionPane.showMessageDialog(null, msg, topic, JOptionPane.ERROR_MESSAGE);
	}
	
	static void display(String topic, String msg)
	{
		JOptionPane.showMessageDialog(null, msg, topic, JOptionPane.INFORMATION_MESSAGE);
	}
	
	public static void display(String title, String text, int row, int col)
	{
		JTextArea s = new JTextArea(text, row, col);
		s.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 14));
		JScrollPane pane = new JScrollPane(s);
		JOptionPane.showMessageDialog(null, pane, title, JOptionPane.INFORMATION_MESSAGE);
	}
}