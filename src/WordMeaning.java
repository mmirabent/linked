public class WordMeaning
{
	String word;
	String wordMeaning;
	
	public WordMeaning(String word, String definition)
	{
		this.word = word;
		this.wordMeaning = definition;
	}
	
	public String getWord()
	{
		return word;
	}
	
	public String getMeaning()
	{
		return wordMeaning;
	}
}